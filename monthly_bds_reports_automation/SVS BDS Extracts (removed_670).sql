--Step 1: check to be sure that all of the BDS materialized views are refreshed:
select bdssetid, short_name, long_name, data_create_dt
 from rr_bds.bds_dfl_sets where set_type ='BDS'
 and set_retired_dt is null
 and SOCIETY_ID = 1
 order by 1,4;
 
 --If you're looking for a dataset that isn't a core BDS, here's the total list:
 select bdssetid, short_name, long_name, data_create_dt
 from rr_bds.bds_dfl_sets 
 where set_retired_dt is null
 order by 1,4;
 
 -- to see which datasets have to have IDE_other set to null:
 select bdssetid, gen_view_name, field_code, setfield_retired_dt 
from rr_bds.bds_dfl_field_comb_mv where field_code = 'IDE_OTHER'
and master_set_type = 'BDS'
order by bdssetid;
 

--Step 2: check to be sure you have a complete lists of non-US centers. If there are any centers listed not in this list, 
-- you'll need to update the list in the US-Only datesets below @ line 200.
--'33:454:477:484:580:611:670:701:962'
select distinct centerid
from vsgnne.reg_center
where country_code <> 'US'
order by 1 asc;


--Step 3: run the regular PI DDs and retrieve them:
--file location:  Oracle/home/DataManagement/bds_extracts
exec rr_bds.pkgbds_dict.create_bds_dict(10,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(11,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(12,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(13,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(18,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(19,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(16,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(17,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(14,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(15,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(24,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(25,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(28,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(29,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(30,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(31,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(40,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(41,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(46,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(47,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(36,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(37,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(38,'N','N');
exec rr_bds.pkgbds_dict.create_bds_dict(39,'N','N');

--Step 4: run the Sensitive DDs and retrieve them:
exec rr_bds.pkgbds_dict.create_bds_dict(10,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(11,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(12,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(13,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(18,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(19,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(16,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(17,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(14,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(15,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(24,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(25,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(28,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(29,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(30,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(31,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(40,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(41,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(46,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(47,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(36,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(37,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(38,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(39,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(54,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(55,'Y','N');
exec rr_bds.pkgbds_dict.create_bds_dict(71,'Y','N');
 ---<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

--Step 5: This will take hours and tie up SQL developer. Highlight all the way to the bottom, then run script. When it's done, retrieve all of the files.

--National, PSO set - Sensitive and all IDE:
exec rr_bds.pkgbds_extract.create_bds2('CEA_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',10,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CEA_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',11,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('EVAR_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',12,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('EVAR_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',13,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('INFRA_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',15,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('INFRA_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',14,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',16,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',17,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',18,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',19,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_PSO_LTF_RETX_Coll',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',54,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_PSO_LTF_STENTS_Coll',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',55,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',28,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',29,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',30,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',31,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',36,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',37,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',38,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',39,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',40,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',41,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CAS_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',46,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CAS_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',47,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('PVI_PSO_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',24,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('PVI_PSO_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',25,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('PVI_PSO_PRO',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',71,'Y',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CEA_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',10,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CEA_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',11,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('EVAR_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',12,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('EVAR_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',13,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('INFRA_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',15,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('INFRA_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',14,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',16,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',17,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',18,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',19,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',28,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',29,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',30,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',31,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',36,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',37,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',38,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',39,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',40,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',41,'N',NULL,670,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CAS_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',46,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('CAS_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',47,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('PVI_International_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',24,'N',NULL,670,'1');

exec rr_bds.pkgbds_extract.create_bds2('PVI_International_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',25,'N',NULL,670,'1');

--
exec rr_bds.pkgbds_extract.create_bds2('CEA_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',10,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('CEA_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',11,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('EVAR_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',12,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('EVAR_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',13,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('INFRA_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',15,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('INFRA_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',14,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',16,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',17,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',18,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',19,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',28,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',29,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',30,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',31,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',36,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',37,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',38,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',39,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',40,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',41,'N',NULL,'33:611:477:454:580:484:701:670:962',NULL);

exec rr_bds.pkgbds_extract.create_bds2('CAS_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',46,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('CAS_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',47,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('PVI_US-Only_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',24,'N',NULL,'33:611:477:454:580:484:701:670:962','1');

exec rr_bds.pkgbds_extract.create_bds2('PVI_US-Only_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),2,NULL,'N',25,'N',NULL,'33:611:477:454:580:484:701:670:962','1');


exec rr_bds.pkgbds_extract.create_bds2('PVI_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',24,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('PVI_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',25,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('CEA_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',10,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CEA_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',11,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('EVAR_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',12,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('EVAR_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',13,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('INFRA_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',15,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('INFRA_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',14,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',16,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('TEVAR_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',17,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',18,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AVACCESS_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',19,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',28,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('SUPRA_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',29,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',30,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('OPEN_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',31,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',36,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('AMP_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',37,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',38,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('VV_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',39,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',40,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('IVC_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',41,'N',NULL,NULL,NULL);

exec rr_bds.pkgbds_extract.create_bds2('CAS_VSGNE_PROC',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',46,'N',NULL,NULL,'1');

exec rr_bds.pkgbds_extract.create_bds2('CAS_VSGNE_LTF',to_date('01/01/1900','DD/MM/YYYY'),trunc(sysdate),21,NULL,'N',47,'N',NULL,NULL,'1');
